% sta�e
clear;

d_r=0.002;%promien drutu
z_r=0.05;%promien zwoju
m=0.01; %g masa
g=9.80665;% przyspieszenie ziemskie
N=2:1:50; %liczba zwoj�w Rafa� Osadnik
G=80*10^9; % 80GPa modu� sztywnosci sprezyny
t=2; %stala czsowa 
q=3;%faza poczatkowa
beta=(1/(2*t));
F=m*g;
alfa=F/m;
freq+(0:0.01:400);

%Loop for is idea Julia Pluta
for i = 1:size(N,2)
    %Spring constant
    k=(G*d_r^4)/(4*N*z_r^3);
    %Resonant frequency
    omega = sqrt(k / m); 
    %Calculate the amplitude and resonance frequency
    for j = 1:size(freq,2)
        A(j,i) = alpha/sqrt((omega^2 - freq(j)^2)^2 + (4 * beta^2 * freq(j)^2));
    end
    [peak(i), index] = max(A(:,i));
    psi(i) = freq(index)/2/pi;
    %Creating the first chart.
    subplot(2,1,1); 
    plot(freq/2/pi,A)
end

xlim([0 80])
xlabel('Hz');
ylabel('[m]');

subplot(2,1,2);
plot(N,peak);

xlim([0 55])
xlabel('N');
ylabel('Max [m]');

saveas(gcf,'rezonans.pdf')



FileID = fopen('wyniki_rezonansu.dat','w'); % zapisanie do pliku

fprintf(FileID,'r = %f [m];R = %f [m];m = %f [kg];Tau = %f [s];g = %f [m/s^2];G = %.2d [Pa] \n \n',d_r,z_r,m,t,g,G); %Nag��wki

for i 1:size(N,2)
	fprintf(FileID'N = %.0f, A = %.4f [m], f = %.4f [Hz]\n',N(i),peak(i),psi(i));
end


fclose(FileID);





